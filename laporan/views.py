from django.shortcuts import render
from django.urls import reverse
from django.db import connection
from django.contrib import messages
from django.http import HttpResponseRedirect

# Create your views here.

def laporan(request):
	response = {}
	cursor = connection.cursor()
	cursor.execute('set search_path to bike_sharing')
	cursor.execute("SELECT l.id_laporan, l.datetime_pinjam, p.nama, l.no_kartu_anggota, pj.denda, l.status FROM person as p, laporan as l, peminjaman as pj, anggota as a WHERE p.ktp = a.ktp and l.no_kartu_anggota = a.no_kartu and l.datetime_pinjam = pj.datetime_pinjam and l.nomor_sepeda = pj.nomor_sepeda;")
	retrieve = cursor.fetchall()
	response['laporan'] = retrieve
	print(response)
	return render(request, 'laporan.html', response)
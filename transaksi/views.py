from django.shortcuts import render
from django.urls import reverse
from django.db import connection
from django.contrib import messages
from django.http import HttpResponseRedirect

# Create your views here.

def transaksi(request):
	response = {}
	cursor = connection.cursor()
	cursor.execute('set search_path to bike_sharing')
	cursor.execute("SELECT tr.date_time, tr.jenis, tr.nominal FROM transaksi as tr, anggota as a, person as p WHERE a.ktp = p.ktp and tr.no_kartu_anggota = a.no_kartu;")
	retrieve = cursor.fetchall()
	response['transaksi'] = retrieve
	print(response)
	return render(request, 'transaksi.html', response)
	
def topup(request):
	return render (request, 'topup.html')
	
def topup_act(request):
	response = {}
	cursor.execute('set search_path to bike_sharing')
	if(request.method == "POST"):
		saldo_topup = request.POST['topup']
		cursor.execute("SELECT a.saldo FROM anggota as a WHERE a.ktp = '"+ktp+"' ")
		saldo = cursor.fetchall()
		for i in saldo:
			saldo = i[0]
		saldo_new = int(saldo_topup) + int(saldo)
		cursor.execute("UPDATE anggota SET saldo = '"+ str(saldo_akhir) +"' WHERE a.ktp = '"+ktp+"' ")
		return HttpResponseRedirect('/transaksi/topup')
	else:
		messages.error(request, "Top up transaction cant be done")
		return HttpResponseRedirect('/transaksi/')
		
def topup_list(request):
	response = {}
	cursor = connection.cursor()
	cursor.execute('set search_path to bike_sharing')
	cursor.execute("SELECT tr.date_time, tr.jenis, tr.nominal FROM transaksi as tr, anggota as a, person as p WHERE a.ktp = p.ktp and tr.no_kartu_anggota = a.no_kartu;")
	retrieve = cursor.fetchall()
	response['transaksi'] = retrieve
	print(response)
	return render(request, 'topup.html', response)
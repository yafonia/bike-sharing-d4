from django.urls import path
from django.conf.urls import url
from . import views


app_name = 'transaksi'


urlpatterns = [
    path('transaksi/', views.transaksi, name='transaksi'),
	path('topup/', views.topup, name = 'topup'),
	path('transaksi/topup/', views.topup_act, name = 'topup_act'),
]

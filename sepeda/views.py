from django.shortcuts import render
from django.db import connection
from django.http import HttpResponseRedirect,HttpResponse,JsonResponse
from django.urls import reverse
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt


# Create your views here.
def sepeda(request):
    cursor = connection.cursor()
    cursor.execute('set search_path to bike_sharing,public')
    cursor.execute("SELECT P.nama FROM ANGGOTA as A, PERSON as P WHERE A.ktp = P.ktp;")
    data = cursor.fetchall()

    response = {}
    response ['namapenyumbang'] = data
    print(response)

    cursor.execute("SELECT nama FROM STASIUN;")
    data1 = cursor.fetchall()
    response ['namastasiun'] = data1
    print(response)

    return render(request, 'sepeda.html', response)

def list_sepeda(request):
    cursor = connection.cursor()
    cursor.execute('SELECT Se.nomor,Se.merk,Se.jenis,status,Se.id_stasiun,Se.no_kartu_penyumbang FROM BIKE_SHARING.SEPEDA Se, BIKE_SHARING.STASIUN St, BIKE_SHARING.ANGGOTA a WHERE a.no_kartu = Se.no_kartu_penyumbang AND Se.id_stasiun = St.id_stasiun;')
    res = cursor.fetchall()
    response = {}
    response['sepeda'] = res
    return render(request, 'list_sepeda.html', response)

def post_sepeda(request):
    if(request.method=="POST"):
          
        merk = request.POST['merk']
        jenis = request.POST['jenis']
        status = request.POST['status']
        stasiun = request.POST['stasiun']
        penyumbang = request.POST['penyumbang']
      
        statusNow = False
        if(str(status)=='Tersedia'):
            statusNow = True
        else:
            statusNow = False

        cursor = connection.cursor()
        cursor.execute('SET SEARCH_PATH to BIKE_SHARING,public')
        cursor.execute("select a.no_kartu from anggota as a, person as p where p.nama='"+penyumbang+"' and p.ktp = a.ktp;") 
        res_penyumbang = cursor.fetchone()
        no_kartu_penyumbang = res_penyumbang[0]
        
        cursor.execute('SET SEARCH_PATH to BIKE_SHARING,public')
        cursor.execute("SELECT id_stasiun FROM STASIUN WHERE nama='"+stasiun+"';")
        res_stasiun = cursor.fetchone()
        id_stasiun = res_stasiun[0]
    

        cursor.execute("select nomor from sepeda order by cast (nomor as int) desc;")
        res_sepeda = cursor.fetchone()
        nomor = int(res_sepeda[0]) +1

        cursor.execute("INSERT INTO sepeda(nomor,merk,jenis,status,id_stasiun,no_kartu_penyumbang) values("+"'"+str(nomor)+"','" +str(merk)+"','" + str(jenis) +"','" + str(statusNow) +"', '" + str(id_stasiun) +"', '" + str(no_kartu_penyumbang) +"' );")
        messages.error(request, "success")
        return HttpResponseRedirect('/sepeda/list_sepeda')

    else:
        messages.error(request, "gagal")
        return HttpResponseRedirect('/sepeda/sepeda')

@csrf_exempt
def delete_sepeda(request):
    if(request.method=="POST"):
        nomor= request.POST['id']
        cursor = connection.cursor()
        cursor.execute('set search_path to bike_sharing,public')
        cursor.execute("delete from sepeda where nomor='"+nomor+"';")
        messages.error(request, "success")
        return HttpResponseRedirect('/sepeda/list_sepeda')
    else:
        messages.error(request, "gagal")
        return HttpResponseRedirect('/sepeda/list_sepeda')

@csrf_exempt
def modal_update_sepeda(request):
    if(request.method=="POST"):
        response={}
        sepeda= request.POST['id']
        cursor = connection.cursor()
        cursor.execute('set search_path to bike_sharing,public')
        cursor.execute("select * from sepeda where nomor='"+sepeda+"';")
        stasiun = cursor.fetchone()
     
        cursor.execute("select s.nama from stasiun as s, sepeda as a where a.nomor ='"+sepeda+"' and a.id_stasiun = s.id_stasiun;")
        nama_stasiun = cursor.fetchone()
        
        cursor.execute("select p.nama from person as p, anggota as a, sepeda as n where n.nomor='"+sepeda+"' and a.no_kartu=n.no_kartu_penyumbang and p.ktp=a.ktp;")
        res_penyumbang = cursor.fetchone()
        nama_penyumbang = res_penyumbang[0]

        cursor.execute("select jenis from sepeda where nomor='"+sepeda+"';")
        jenis = cursor.fetchone()

        cursor.execute("select status from sepeda where nomor='"+sepeda+"';")
        status = cursor.fetchone()

        cursor.execute("select merk from sepeda where nomor='"+sepeda+"';")
        merk = cursor.fetchone()

        cursor.execute("select p.nama from person as p, anggota as a where a.ktp=p.ktp")
        penyumbang = cursor.fetchall()

        cursor.execute("select nama from stasiun")
        stasiun = cursor.fetchall()

        response['stasiun'] = stasiun
        response['penyumbang'] = penyumbang
        response['sepeda'] = sepeda
        response['nama_stasiun'] = nama_stasiun
        response['nama_penyumbang'] = nama_penyumbang
        response['jenis'] = jenis[0]
        response['merk'] = merk[0]
        response['status'] = status[0]

        messages.error(request, "success")
        return JsonResponse(response)

    else:
        messages.error(request, "gagal")
        return HttpResponseRedirect('/sepeda/list_sepeda')

@csrf_exempt
def update_sepeda(request):
    if(request.method=="POST"):
        merk = request.POST['merk']    
        jenis = request.POST['jenis']
        status = request.POST['status']
        stasiun = request.POST['stasiun']
        penyumbang = request.POST['penyumbang']
        nomor = request.POST['nomor']

        current_status = False
        if(str(status)=='Tersedia'):
          current_status = True
        else:
          current_status = False

        cursor = connection.cursor()
        cursor.execute('set search_path to bike_sharing,public')

        cursor.execute("select a.no_kartu from anggota as a, person as p where p.nama='"+penyumbang+"' and p.ktp = a.ktp")
        hasil = cursor.fetchone()
        no_kartu = hasil[0]

        cursor.execute("update sepeda set merk='"+merk+"', jenis='"+jenis+"', status='"+str(current_status)+"', no_kartu_penyumbang='"+no_kartu+"' where nomor = '"+str(nomor)+"';")
    
        messages.error(request, "success")
        return HttpResponseRedirect('/sepeda/list_sepeda')
    else:
        messages.error(request, "gagal")
        return HttpResponseRedirect('/sepeda/list_sepeda')



       

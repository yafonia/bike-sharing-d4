from django.urls import path
from django.conf.urls import url
from . import views
from .views import sepeda

app_name = 'sepeda'


urlpatterns = [
    path('sepeda/', views.sepeda, name='sepeda'),
    path('sepeda/list_sepeda/', views.list_sepeda, name='list_sepeda'),
    path('sepeda/post_sepeda/',views.post_sepeda, name = 'post_sepeda'),
    path('sepeda/delete_sepeda/',views.delete_sepeda, name = 'delete_sepeda'),
    path('sepeda/list_sepeda/modal_update_sepeda',views.modal_update_sepeda, name = 'modal_update_sepeda'),
    path('sepeda/list_sepeda/modal_update_sepeda/update_sepeda',views.update_sepeda, name = 'update_sepeda'),
]
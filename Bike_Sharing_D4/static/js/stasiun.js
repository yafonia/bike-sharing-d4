function delete_list_stasiun(x){
    console.log("bisa")
    $.ajax(
        {
          url: "/stasiun/delete_stasiun/",
            datatype: 'json',
            data: {'id': x},
          method:'POST',
          success: function(result){
            location.reload()
            }  
        }
        );
}

function update_modal_stasiun(x){
  $(".modal-body").attr('id',x) 
 
  $.ajax(
      {
        url: "/stasiun/list_stasiun/modal_update_stasiun",
          datatype: 'json',
          data: {'id':x},
        method:'POST',
        success: function(result){
        
          console.log(result)
          console.log("masuk update modal")
          stasiun=''
          for(i=0; i<result.stasiun.length;i++){
              stasiun+='<option>'+result.stasiun[i]+'</option>'
          }
          $('#namaStasiun').attr('placeholder',result.stasiun[4])
          $('#alamatStasiun').attr('placeholder',result.stasiun[1])
          $('#Latitude').attr('placeholder',result.stasiun[2])
          $('#Longitude').attr('placeholder',result.stasiun[3])
        
          }  
      }
      );

}

function update_stasiun(){
  id_stasiun = $(".modal-body").attr('id')
  nama = $('#namaStasiun').val()
  if(nama==''){
      nama = $('#namaStasiun').attr('placeholder')
  }
  alamat = $('#alamatStasiun').val()
  if(alamat==''){
      alamat=$('#alamatStasiun').attr('placeholder')
  }
  latitude = $('#Latitude').val()
  if(latitude==''){
      latitude = $('#Latitude').attr('placeholder')
  }
  longitude = $('#Longitude').val()
  if(longitude==''){
      longitude = $('#Longitude').attr('placeholder')
  }
 

  $.ajax(
      {
        url: "/stasiun/list_stasiun/modal_update_stasiun/update_stasiun",
          datatype: 'json',
          data: {
              'id_stasiun':id_stasiun,
              'alamat':alamat,
              'latitude':latitude,
              'longitude':longitude,
              'nama':nama,
          },
        method:'POST',
        success: function(result){
          location.reload()
          } ,
          error:function(a){
              alert(a)
              location.reload();

          }
         
      }
  );

}

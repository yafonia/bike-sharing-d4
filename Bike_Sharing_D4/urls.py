"""Bike_Sharing_D4 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
import home.urls as home
import voucher.urls as voucher
import acara.urls as acara
import penugasan.urls as penugasan
import peminjaman.urls as rent
import sepeda.urls as sepeda
import stasiun.urls as stasiun

import account.urls as account
import transaksi.urls as transaksi
import laporan.urls as laporan



import transaksi.urls as transaksi
from home import views
from voucher import views
from penugasan import views
from acara import views
from peminjaman import views
from sepeda import views
from stasiun import views
from account import views
from transaksi import views
from laporan import views


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(home)),
    path('', include(voucher, namespace = 'voucher')),
	path('', include(acara, namespace = 'acara')),
	path('', include(penugasan, namespace = 'penugasan')),
    path('', include(rent, namespace = 'rent')),
    path('', include(sepeda, namespace = 'sepeda')),
    path('', include(stasiun, namespace = 'stasiun')),
	path('', include(account, namespace = 'account')),
    path('', include(transaksi, namespace = 'transaksi')),
    path('', include(laporan, namespace = 'laporan')),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

from django.shortcuts import render
from django.db import connection
from django.http import HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt

# Create your views here.

def rent(request):
    response={}
    cursor = connection.cursor()

    cursor.execute('set search_path to bike_sharing,public')

    cursor.execute("SELECT jenis FROM sepeda;")
    res = cursor.fetchall()
    response['bikeinfo'] = res

    return render(request, 'rent.html',response)

def list_rent(request):
    response={}
    cursor = connection.cursor()

    cursor.execute('set search_path to bike_sharing,public')
    cursor.execute("SELECT no_kartu_anggota, sp.jenis, st.nama, datetime_kembali, biaya, denda FROM peminjaman rent, sepeda sp, stasiun st WHERE rent.nomor_sepeda = sp.nomor AND rent.id_stasiun = st.id_stasiun;")
    res = cursor.fetchall()
    response['rent'] = res

    return render(request, 'list_rent.html', response)


@csrf_exempt
def delete_rent(request):
    if(request.method=="POST"):
        nomor = request.POST['id']
        cursor = connection.cursor()
        cursor.execute('set search_path to bike_sharing,public')
        cursor.execute("delete from voucher where id_voucher='"+nomor+"';")
        messages.error(request, "success")
        return HttpResponseRedirect('/rent/')
    else:
        messages.error(request, "failed")
        return HttpResponseRedirect('/rent/')
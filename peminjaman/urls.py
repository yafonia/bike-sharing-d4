from django.urls import path
from django.conf.urls import url
from . import views
from .views import rent


app_name = 'rent'


urlpatterns = [
    path('rent/', views.rent, name='rent'),
    path('rent/list_rent/', views.list_rent, name='list_rent'),
    path('rent/delete_rent/', views.delete_rent, name='delete_rent')
]

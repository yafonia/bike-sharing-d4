from django.urls import path
from django.conf.urls import url
from . import views
from .views import acara

app_name = 'acara'


urlpatterns = [
    path('acara/', views.acara, name='acara'),
    path('acara/list_acara/', views.list_acara, name='list_acara'),
]
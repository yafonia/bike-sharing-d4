from django.shortcuts import render
from django.db import connection
from django.http import HttpResponseRedirect,HttpResponse,JsonResponse
from django.urls import reverse
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt

# Create your views here.
def acara(request):
    response = {}
    cursor = connection.cursor()
    cursor.execute('set search_path to bike_sharing,public')
    cursor.execute("SELECT nama FROM STASIUN;")
    data1 = cursor.fetchall()

    response ['namastasiun'] = data1
    print(response)    
    return render(request, 'acara.html', response)


def list_acara(request):
    response={}
    cursor = connection.cursor()

    cursor.execute('set search_path to bike_sharing,public')
    cursor.execute("SELECT ac.judul, ac.deskripsi, ac.is_free, ac.tgl_mulai, ac.tgl_akhir, st.id_stasiun, st.nama FROM acara ac, stasiun st, acara_stasiun acs WHERE acs.id_stasiun=st.id_stasiun AND acs.id_acara=ac.id_acara;")
    res = cursor.fetchall()
    response['acara'] = res
    return render(request, 'list_acara.html', response)



from django.shortcuts import render
from django.db import connection
from django.http import HttpResponseRedirect

# Create your views here.

def penugasan(request):
    response = {}
    cursor = connection.cursor()
    cursor.execute('set search_path to bike_sharing,public')
    cursor.execute("SELECT nama FROM STASIUN;")
    data1 = cursor.fetchall()
    response ['namastasiun'] = data1
    print(response)    

    return render(request, 'penugasan.html', response)

def list_penugasan(request):
    response={}
    cursor = connection.cursor()

    cursor.execute('set search_path to bike_sharing,public')
    cursor.execute("SELECT pn.ktp, pr.nama, pn.start_datetime, pn.end_datetime, pn.id_stasiun, st.nama FROM stasiun st, person pr, penugasan pn, petugas pt WHERE st.id_stasiun = pn.id_stasiun AND pn.ktp=pt.ktp AND pt.ktp=pr.ktp;")
    res = cursor.fetchall()
    response['penugasan'] = res

    return render(request, 'list_penugasan.html', response)
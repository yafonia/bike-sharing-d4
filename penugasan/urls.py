from django.urls import path
from django.conf.urls import url
from . import views
from .views import penugasan

app_name = 'penugasan'


urlpatterns = [
    path('penugasan/', views.penugasan, name='penugasan'),
    path('penugasan/list_penugasan', views.list_penugasan, name='list_penugasan')
]
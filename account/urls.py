from django.urls import path
from django.conf.urls import url
from . import views
from .views import register
from .views import login

app_name = 'account'


urlpatterns = [
    path('register/', views.register, name='register'),
    path('login/', views.login, name='login'),
]

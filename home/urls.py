from django.urls import path, include
from django.contrib.auth import views
from .views import home
from django.conf import settings

app_name = 'home'

urlpatterns = [
 path('', home, name='home'),
]


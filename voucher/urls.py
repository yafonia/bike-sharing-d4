from django.urls import path
from django.conf.urls import url
from . import views
from .views import voucher, update, post_voucher, form_voucher, delete_voucher


app_name = 'voucher'

urlpatterns = [
    path('voucher/', views.form_voucher, name='form_voucher'),
    path('update/', views.update, name='update'),
    path('voucher/post_voucher/', views.post_voucher, name='post_voucher'),
    path('voucher/list_voucher/', views.voucher, name='voucher'),
    path('voucher/delete_voucher/', views.delete_voucher, name='delete_voucher')
]

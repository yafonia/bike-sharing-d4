from django.shortcuts import render
from django.db import connection
from django.http import HttpResponseRedirect
from django.contrib import messages
from random import randint
from django.views.decorators.csrf import csrf_exempt

# Create your views here.

def update(request):
	return render(request, 'update_voucher.html')

def form_voucher(request):
    return render(request, 'form_voucher.html')

@csrf_exempt
def delete_voucher(request):
    if(request.method=="POST"):
        nomor = request.POST['id']
        cursor = connection.cursor()
        cursor.execute('set search_path to bike_sharing,public')
        cursor.execute("delete from voucher where id_voucher='"+nomor+"';")
        messages.error(request, "success")
        return HttpResponseRedirect('/voucher/list_voucher')
    else:
        messages.error(request, "failed")
        return HttpResponseRedirect('/voucher/list_voucher')

def voucher(request):
    response={}
    cursor = connection.cursor()

    cursor.execute('set search_path to bike_sharing,public')
    cursor.execute("SELECT * from voucher;")
    res = cursor.fetchall()
    response['voucher'] = res
    return render(request, 'voucher.html',response)

def post_voucher(request):
    if(request.method == 'POST'):
        nama = request.POST['nama']
        kategori = request.POST['kategori']
        nilai_poin = request.POST['nilai_poin']
        deskripsi = request.POST['deskripsi']
        jml_voucher = request.POST['jml_voucher']
        cursor = connection.cursor()
        cursor.execute('set search_path to bike_sharing,public')
        cursor.execute("SELECT id_voucher FROM voucher")
        id_voucher = cursor.fetchall()
        for row in id_voucher:
            id_voucher = row[0]
        

        cursor.execute("SELECT id_voucher FROM voucher ORDER BY cast (id_voucher as int) DESC;")
        res = cursor.fetchone()
        id_voucher = int(res[0])
        for i in range(0,int(jml_voucher)):
            idv = str(id_voucher + 1 + i)
            print(id_voucher)
            cursor.execute("INSERT INTO voucher(id_voucher, nama, kategori, nilai_poin, deskripsi) values("+"'"+str(idv)+"','" +nama+"','" +kategori+"','" + nilai_poin + "','" +deskripsi+"');")
        
        messages.error(request, "success")
        return HttpResponseRedirect('/voucher/list_voucher')
    else:
        messages.error(request, "failed")
        return HttpResponseRedirect('/voucher')

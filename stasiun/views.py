from django.shortcuts import render
from django.db import connection
from django.http import HttpResponseRedirect,HttpResponse,JsonResponse
from django.urls import reverse
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt


# Create your views here.
def stasiun(request):
    cursor = connection.cursor()
    cursor.execute('set search_path to bike_sharing')
    cursor.execute("SELECT nama FROM STASIUN")
    data = cursor.fetchall()

    response = {}
    response ['namastasiun'] = data
    print(response)
    return render(request, 'stasiun.html', response)


def list_stasiun(request):
    cursor = connection.cursor()
    cursor.execute('SELECT * FROM BIKE_SHARING.STASIUN')
    res = cursor.fetchall()
    response = {}
    response['stasiun'] = res
    return render(request, 'list_stasiun.html', response)

def post_stasiun(request):
    if(request.method=="POST"):
        nama = request.POST['nama']    
        alamat = request.POST['alamat']
        longitude = request.POST['longitude']
        latitude = request.POST['latitude']
      
        cursor = connection.cursor()
        cursor.execute('set search_path to bike_sharing,public')
        cursor.execute("SELECT id_stasiun FROM BIKE_SHARING.STASIUN")
        id_stasiun = cursor.fetchall()
        for row in id_stasiun:
            id_stasiun = row[0]
        
        cursor.execute("SELECT id_stasiun FROM stasiun ORDER BY id_stasiun DESC;")
        res = cursor.fetchone()
        id_stasiun = res[0] + "A"
   
        print(id_stasiun)
        cursor.execute("INSERT INTO stasiun(id_stasiun,alamat,lat,long,nama) VALUES("+"'"+str(id_stasiun)+"','" +str(alamat)+"'," + str(latitude) +",'" + str(longitude) +"','" + str(nama)+"');")
        messages.error(request, "success")
        return HttpResponseRedirect('/stasiun/list_stasiun')
    else:
        messages.error(request, "gagal")
        return HttpResponseRedirect('/stasiun/stasiun')


@csrf_exempt
def delete_stasiun(request):
    if(request.method=="POST"):
        id_stasiun = request.POST['id']
        cursor = connection.cursor()
        cursor.execute('set search_path to bike_sharing,public')
        cursor.execute("delete from stasiun where id_stasiun='"+id_stasiun+"';")
        messages.error(request, "success")
        return HttpResponseRedirect('/stasiun/list_stasiun')
    else:
        messages.error(request, "gagal")
        return HttpResponseRedirect('/stasiun/list_stasiun')

@csrf_exempt
def modal_update_stasiun(request):
    if(request.method=="POST"):
        response={}

        stasiun= request.POST['id']
        cursor = connection.cursor()
        cursor.execute('set search_path to bike_sharing,public')
        cursor.execute("select * from stasiun where id_stasiun='"+stasiun+"';")
        stasiun = cursor.fetchone()
       
        response['stasiun'] = stasiun
        messages.error(request, "success")
        return JsonResponse(response)

    else:
        messages.error(request, "gagal")
        return HttpResponseRedirect('stasiun/list_stasiun/')

@csrf_exempt
def update_stasiun(request):
    if(request.method=="POST"):
        nama = request.POST['nama']    
        alamat = request.POST['alamat']
        longitude = request.POST['longitude']
        latitude = request.POST['latitude']
        id_stasiun= request.POST['id_stasiun']

        cursor = connection.cursor()
        cursor.execute('set search_path to bike_sharing,public')
        cursor.execute("update stasiun set nama='"+nama+"', alamat='"+alamat+"', lat='"+latitude+"', long='"+longitude+"' where id_stasiun = '"+id_stasiun+"';")
      
        messages.error(request, "success")
        return HttpResponseRedirect('/stasiun/list_stasiun')
    else:
        messages.error(request, "gagal")
        return HttpResponseRedirect('/stasiun/list_Stasiun')




from django.urls import path
from django.conf.urls import url
from . import views


app_name = 'stasiun'


urlpatterns = [
    path('stasiun/', views.stasiun, name='stasiun'),
    path('stasiun/list_stasiun/', views.list_stasiun, name='list_stasiun'),
    path('stasiun/post_stasiun/', views.post_stasiun, name = 'post_stasiun'),
    path('stasiun/delete_stasiun/',views.delete_stasiun, name = 'delete_stasiun'),
    path('stasiun/list_stasiun/modal_update_stasiun',views.modal_update_stasiun, name = 'modal_update_stasiun'),
    path('stasiun/list_stasiun/modal_update_stasiun/update_stasiun',views.update_stasiun, name = 'update_stasiun'),
]